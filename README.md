# App Installer

App installer for the apps I use regularly on a pacman-based system (e.g. Manjaro, Arch)
Run updates and reboot prior to installing all the things!

```
pacman -Syyu
pacman -S - < pkglist.txt
yay -S - < yaylist.txt
pip install -r piplist.txt
code --install-extension [extension]
```

## Notes for install-helper.py:
Uses wget module for installation of the Custom Proton for Steam.
`pip install -r requirements.txt`

VSCode requires one-at-a-time installation of extensions.
The python script in this repo will handle this.
```
python install-helper.py
```

## Configure git
If planning to do dev stuff, initialize the git global config
using the install-helper python script. I use github as well as gitlab so this 
will handle the creation of multiple SSH keys as well as an SSH
config file, checking if one already exists first.
GitLab line in config file:
`UpdateHostKeys no`
is to suppress the error:
`client_global_hostkeys_private_confirm: server gave bad signature for RSA key 0`
Remove this line if it causes problems.


## Notes for specific packages:
### Spelling check packages
### suppress info errors when launching Kate editor
`aspell
hunspell-en_US
hspell
libviokko`
### DB Drivers for GNUCash
`libdbi
libdbi-drivers`
### Packages required to enable gestures for touchpad
`xdotool
xf86-input-synaptics
libinput-gestures`
### KDE-Specific Widget for setting up like Mac-like dock
`latte-dock`
### KDE-Specific Widget - Configures Title bar in Global Menu
### https://www.dedoimedo.com/computers/plasma-look-like-mac.html
`plasma5-applets-active-window-control`


## Post-installation Tasks:
### Set up latte dock and widgets
If going for the dock / top menu bar look, search the apps menu for dock and add it.
Drag the menu bar from the bottom to the top and add the global menu, active window control, and a spacer (L-to-R). Edit the settings of
active window control so it shows the Minimize and Maximize buttons.
Run this to remove the top title bar when windows are maximized:
`kwriteconfig5 --file ~/.config/kwinrc --group Windows --key BorderlessMaximizedWindows true
qdbus org.kde.KWin /KWin reconfigure`
[Reference for removing title bar](https://askubuntu.com/questions/253337/remove-title-bar-and-borders-on-maximized-windows-in-kubuntu)

### initialize postgres:
### https://lobotuerto.com/blog/how-to-install-postgresql-in-manjaro-linux/
### restore database dumps manually
###    e.g.:
```
psql -U justin -d gnucash-kff -f ./Desktop/dumpfile 
```

### set up cron jobs, e.g. [./dev/pg-db-backup/db-export.py]

### Install/configure Oh My Zsh via install-helper.py

### Ensure OMZ Plugins are working - they should be if the install_omz function in install-helper ran properly.

### Mounting an NFS Share
Substitute the relevant server and local share directory
as necessary. Optionally backup /etc/fstab
```
sudo cp /etc/fstab /etc/fstab_bkp
```
and add the following line:
192.168.100.100:/mnt/tank /home/justin/nfs_share nfs _netdev 0 0

### Restoring a user profile
Navigate to user directory and run:
```
tar xzvf /backup/location/mybackup.tar.gz
```

### Setting up .pypirc file
Use template provided and plug in keys from gitlab
and pypi, place in home directory for easy deployments

### Accessing USB Devices / Microcontrollers without sudo:
Find the device:
`sudo dmesg | grep tty`
Find the Group ID by running this command: (change tty device accordingly)
`stat /dev/ttyUSB0`
Add your Linux user to the gid determined in the previous command ("uucp" in this case)
`sudo gpasswd -a $USER uucp`

### Issues I've encountered + Fixes
| Issue | Fix |
| - |:- |
| Blocky, weird looking text rendered in PDFs and Webpages | epifonts was causing a conflict with Helvetica (`yay -R epifonts`)|
| Spotify Problem importing key | Run this: `curl -sS https://download.spotify.com/debian/pubkey.gpg \| gpg --import -` and choose No when yay asks to import key during installation |
